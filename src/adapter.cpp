#include <memory>
#include <list>

#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/NavSatFix.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "fiducial_msgs/FiducialTransformArray.h"

#include "autonomous_driving/AutonomousDriver.h"
#include "autonomous_driving/control/Actuation.h"
#include "autonomous_driving/control/Control.h"
#include "autonomous_driving/control/RoverMovementControl.h"
#include "autonomous_driving/sense/Perception.h"
#include "autonomous_driving/sense/GPSSense.h"
#include "autonomous_driving/sense/MagneticSense.h"
#include "autonomous_driving/sense/ArucoSense.h"

#include <iostream>
using namespace std;


std::unique_ptr<AutonomousDriver> autonomousDriver;

GPSSense lastGPSFix;

void onGPSFixReceived(const sensor_msgs::NavSatFix::ConstPtr& message)
{
    lastGPSFix.latitude = message->latitude;
    lastGPSFix.longitude = message->longitude;
    lastGPSFix.altitude = message->altitude;
}

MagneticSense lastMagneticSense;

void onMagneticReceived(const geometry_msgs::Vector3Stamped::ConstPtr& message)
{
    lastMagneticSense.x = message->vector.x;
    lastMagneticSense.y = message->vector.y;
    lastMagneticSense.z = message->vector.z;
}

ArucoSense lastArucoSense;

void onArucoDetected(const fiducial_msgs::FiducialTransformArray::ConstPtr& message)
{   
    if (message->transforms.size() == 0) { 
        // Not aruco marker detected
        lastArucoSense.id = -1;
    } else {
        lastArucoSense.id = message->transforms[0].fiducial_id;
        lastArucoSense.translation_x = message->transforms[0].transform.translation.x;
        lastArucoSense.translation_y = message->transforms[0].transform.translation.y;
        lastArucoSense.translation_z = message->transforms[0].transform.translation.z;
    }
}


int main(int argc, char** argv)
{    
    ros::init(argc, argv, "autonomous_driver");

    ros::NodeHandle nodeHandle;
    ros::NodeHandle privateNodeHandle("~");

    ros::Publisher roverMovementPublisher = nodeHandle.advertise<geometry_msgs::Twist>("/rover_diff_drive_controller/cmd_vel", 1000);
    ros::Subscriber gpsSubscriber = nodeHandle.subscribe("/gps/fix", 1000, onGPSFixReceived);
    ros::Subscriber magneticSubscriber = nodeHandle.subscribe("/magnetic", 1000, onMagneticReceived);
    ros::Subscriber arucoSubscriber = nodeHandle.subscribe("/fiducial_transforms", 1000, onArucoDetected);

    Actuation roverActuation;
    roverActuation.move = std::make_shared<RoverMovementControl>([&](const RoverMovementControlParameter& wantedMovement) {
        geometry_msgs::Twist message;
        message.linear.x = wantedMovement.linearVelocity;
        message.angular.z = wantedMovement.angularVelocity;
        roverMovementPublisher.publish(message);
    });

    autonomousDriver = std::make_unique<AutonomousDriver>(roverActuation);

    autonomousDriver->runStrategy("aruco_detect");

    ros::Rate loopRate(10);

    while(ros::ok())
    {
        Perception currentPerception;

        currentPerception.gpsSense = lastGPSFix;
        currentPerception.magneticSense = lastMagneticSense;
        currentPerception.arucoSense = lastArucoSense;

        autonomousDriver->update(currentPerception);

        ros::spinOnce();
        loopRate.sleep();
    }

    return 0;
}